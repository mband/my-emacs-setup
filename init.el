(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(bookmark-save-flag 1)
 '(company-backends (quote (company-ycmd company-elisp company-bbdb company-nxml company-css company-eclim company-semantic company-ropemacs company-cmake company-capf (company-dabbrev-code company-gtags company-etags company-keywords) company-oddmuse company-files company-dabbrev)))
 '(global-subword-mode t)
 '(inhibit-startup-screen t)
 '(menu-bar-mode nil)
 '(request-log-level (quote debug))
 '(request-message-level -1)
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil)
 '(ycmd-idle-change-delay 2.5))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "black" :foreground "dark gray" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 113 :width normal :foundry "unknown" :family "DejaVu Sans Mono")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Notes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; company-backends had to be modified to disable the clang backend as it was causing trouble for some reason -> and ycmd / company-ycmd is used for clang completions.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Explicit load packages in the init file to configure them
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq package-enable-at-startup nil)
(package-initialize)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Macro to reduce the code required for installing packages within the custom config configurations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defmacro my-install (pkg)
  `(unless (package-installed-p ',pkg)
     (package-refresh-contents)
     (package-install ',pkg)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Load custom config configurations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'load-path "~/.emacs.d/config")
(load-library "config_core")
(load-library "config_c++")
(load-library "config_company")
(load-library "config_gtags")
(load-library "config_ycmd")
(load-library "config_yaml")
;(load-library "config_slime")
;(load-library "config_slime_ros")
(load-library "config_langtool")
(load-library "config_clang-format")
(load-library "config_magit")
(load-library "config_ros")
(load-library "config_graphviz")
(load-library "config_markdown")

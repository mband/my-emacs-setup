;; [package quicklisp-slime-helper]
;; slime-helper.el installed in "/data/mband/emacs/quicklisp/slime-helper.el"

;; To use, add this to your ~/.emacs:

(load (expand-file-name "/data/mband/emacs/quicklisp/slime-helper.el"))
;; Replace "sbcl" with the path to your implementation
(setq inferior-lisp-program "sbcl")

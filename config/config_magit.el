(my-install magit)

;; To disable the warning message.
(setq magit-last-seen-setup-instructions "1.4.0")

(global-set-key [f1] 'magit-status)

;; Load xml mode when editing ros .launch files
(add-to-list 'auto-mode-alist '("\\.launch\\'" . xml-mode))

(my-install yaml-mode)

;; Load the YAML mode when editing .sls files (Salt state files)
(add-to-list 'auto-mode-alist '("\\.sls\\'" . yaml-mode))

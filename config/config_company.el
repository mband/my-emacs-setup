(my-install company)

; Enable global-company-mode to automatically enable company-mode for all/many major modes (see the function description)
(global-company-mode t)

; Bind M-n to the company-complete command (as there can be some delay with automatic completion when fetching the completions from ycmd (it appears that there has to happen a compilation first, but since they are not frequently invoked anylonger due to some major latency experience, then it can be necessary to manually request the company-complete).
(global-set-key (kbd "M-n") 'company-complete)

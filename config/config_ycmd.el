;; The ycmd backend can be fetched from https://github.com/Valloric/ycmd
;; The emacs-ycmd can be found at https://github.com/abingham/emacs-ycmd

(my-install ycmd)
(my-install company-ycmd)

;; ycmd
(require 'ycmd)
; Use 'cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON' to generate a 'compile_commands.json' file, that should be pointed to in the '.ycm_extra_conf.py' file

; Specify how to run the ycmd server
(set-variable 'ycmd-server-command '("python" "/data/mband/emacs/ycmd/ycmd"))

; Whitelist .ycm_extra_conf.py files (whitelisting every one in the homedirectory could be a security risk, but convenience beats it this time given the low risk)
(set-variable 'ycmd-extra-conf-whitelist '("/data/mband/emacs/ycmd/*" "~/*" "/data/mband/shared_resources/*"))

(setq ycmd--log-enabled t)

; Open ycmd minor-mode on c++ mode
(add-hook 'c++-mode-hook 'ycmd-mode)

; company-ycmd
(require 'company-ycmd)

(my-install langtool)

;; langtool.el --- Grammar check utility using LanguageTool
;;
;;; Install:
;; Install LanguageTool (and java)
;; http://www.languagetool.org/
;; Put this file into load-path'ed directory, and byte compile it if
;; desired. And put the following expression into your ~/.emacs.
;;
;; (require 'langtool)
;; (setq langtool-language-tool-jar "/path/to/LanguageTool.jar")

(require 'langtool)
(setq langtool-language-tool-jar "/data/mband/shared_resources/languagetool/LanguageTool-2.9/languagetool-commandline.jar")

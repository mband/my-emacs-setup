;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; C++ style
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use the following to see a list of indentation rules that can be modified
;; describe-variable <RET> c-offsets-alist <RET>
;; bsd is also known as the allman style
;; Modified to fit clang-format-3.5 with the .clang-format file style setup
(c-add-style "caros"
             '("bsd"
               (indent-tabs-mode . nil)
               (c-basic-offset . 2)
	       (c-offsets-alist . (
				   (innamespace . 0)))))

(c-add-style "robwork"
             '("bsd"
               (indent-tabs-mode . nil)
               (c-basic-offset . 4)
	       (c-offsets-alist . (
				   (innamespace . +)))))

(defun my-c++-mode-hook ()
  (c-set-style "caros"))

(add-hook 'c++-mode-hook 'my-c++-mode-hook)

;; Set c++ mode for .h files (ROS choose coding conventions that requires the user to know when a .h file is C or C++ code)
;; Could probably use a function that looks up the path of the .h file (and if in caros/ROS then use c++-mode)
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

;; (my-install clang-format)

(load "/usr/share/emacs/site-lisp/clang-format-3.5/clang-format.el")
;; (require 'clang-format)
(global-set-key [C-M-tab] 'clang-format-buffer)
